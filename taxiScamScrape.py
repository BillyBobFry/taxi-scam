import csv
import socket
import json

###############################################
# This script takes a list of places in the UK,
# and does DNS lookups against a number of
# taxi-related hostnames for each place. It then
# stores the results of the DNS lookups in a
# JSON file.
###############################################

parsedPlaces = []


def parseUKPlaceRow(r):
    noApostrophePlaceName = r[1].replace("'", "")
    return {"noSpaces": noApostrophePlaceName.replace(" ", ""), "withDashes": noApostrophePlaceName.replace(" ", "-")}


def containsIP(searchList, searchIP):
    for x in searchList:
        if x["ip"] == searchIP:
            return True
    return False


def resolveHostname(prefix, place, suffix):
    hostname = prefix + place + suffix
    ipAddress = socket.gethostbyname(hostname)
    print(hostname)
    if containsIP(matchedIPs, ipAddress):
        for x in matchedIPs:
            if x["ip"] == ipAddress:
                x["count"] = x["count"] + 1
                newPlace = True
                for y in x["places"]:
                    if y == place:
                        newPlace = False
                if newPlace:
                    x["places"].append(place)

                newHostname = True
                for y in x["hostnames"]:
                    if y == hostname:
                        newHostname = False
                if newHostname:
                    x["hostnames"].append(hostname)

    else:
        matchedIPs.append({"ip": ipAddress, "count": 1,
                           "places": [], "hostnames": []})


# open csv
with open("uk_places.csv", newline="") as csvfile:
    # initiate the list of IPs that we have resolved
    matchedIPs = []
    places = csv.reader(csvfile, delimiter=",", quotechar="'")
    next(places)
    parsedPlaces = map(parseUKPlaceRow, places)
    print(parsedPlaces)
    for r in parsedPlaces:
        try:  # try loads of different possible hostnames
            resolveHostname("", r["noSpaces"], "taxis.co.uk")
            resolveHostname("", r["withDashes"], "taxis.co.uk")
            resolveHostname("", r["noSpaces"], "taxi.co.uk")
            resolveHostname("", r["withDashes"], "taxi.co.uk")
            resolveHostname("", r["noSpaces"], "-taxis.co.uk")
            resolveHostname("", r["withDashes"], "-taxis.co.uk")
            resolveHostname("", r["noSpaces"], "-taxi.co.uk")
            resolveHostname("", r["withDashes"], "-taxi.co.uk")
            resolveHostname("", r["noSpaces"], "executivecars.co.uk")
            resolveHostname("", r["withDashes"], "executivecars.co.uk")
            resolveHostname("", r["noSpaces"], "minibuses.co.uk")
            resolveHostname("", r["withDashes"], "minibuses.co.uk")
            resolveHostname("taxis", r["noSpaces"], ".co.uk")
            resolveHostname("taxis", r["withDashes"], ".co.uk")
            resolveHostname("taxi", r["noSpaces"], ".co.uk")
            resolveHostname("taxi", r["withDashes"], ".co.uk")
            resolveHostname("taxis-", r["noSpaces"], ".co.uk")
            resolveHostname("taxis-", r["withDashes"], ".co.uk")
            resolveHostname("taxi-", r["noSpaces"], ".co.uk")
            resolveHostname("taxi-", r["withDashes"], ".co.uk")
            resolveHostname("executivecars", r["noSpaces"], ".co.uk")
            resolveHostname("executivecars", r["withDashes"], ".co.uk")
            resolveHostname("minibuses", r["noSpaces"], ".co.uk")
            resolveHostname("minibuses", r["withDashes"], ".co.uk")

        except Exception as e:
            pass  # we just want to continue

        # sort the list, with the most common IPs appearing first
        if len(matchedIPs) > 0:
            matchedIPs.sort(
                key=lambda i: i["count"], reverse=True)


# enrich data with length of arrays
for ip in matchedIPs:
    ip["placeCount"] = len(ip["places"])
    ip["hostnameCount"] = len(ip["hostnames"])

# output to file
with open("matchedIPs.json", "w") as outfile:
    json.dump(matchedIPs, outfile)

print("here")
